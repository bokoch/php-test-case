<?php

namespace App\Entity;

use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Users
 * @MongoDB\Document(repositoryClass="App\Repository\UserRepository")
 * @MongoDB\HasLifecycleCallbacks
 */
class Users
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="Login should be not blank")
     * @Assert\Length(max=32)
     * @MongoDB\Field(type="string")
     */
    protected $login;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\Length(max=32)
     * @Assert\NotBlank(message="Password should be not blank")
     */
    protected $password;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $lastChange;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login): void
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getLastChange()
    {
        return $this->lastChange;
    }

    /**
     * @MongoDB\PreUpdate
     * @MongoDB\PrePersist
     * Set lastChange before persist document
     */
    public function setLastChange(): void
    {
        $this->lastChange = new DateTime();
    }
}