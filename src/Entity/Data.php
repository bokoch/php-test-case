<?php

namespace App\Entity;

use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Data
 * @MongoDB\Document
 * @MongoDB\HasLifecycleCallbacks
 */
class Data
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * App\Entity\Users entity id
     * @Assert\NotBlank(message="UserId should be not blank")
     * @MongoDB\Field(type="string")
     */
    protected $userId;

    /**
     * @Assert\NotBlank(message="Key should be not blank")
     * @Assert\Length(max=32)
     * @MongoDB\Field(type="string")
     */
    protected $key;

    /**
     * @Assert\NotBlank(message="Data should be not blank")
     * @Assert\Length(max=2048)
     * @MongoDB\Field(type="string")
     */
    protected $data;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $lastChange;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key): void
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getLastChange()
    {
        return $this->lastChange;
    }

    /**
     * @MongoDB\PreUpdate
     * @MongoDB\PrePersist
     * Set lastChange before persist/update document
     */
    public function setLastChange(): void
    {
        $this->lastChange = new DateTime();
    }



}