<?php

namespace App\Controller;

use App\Controller\Contracts\AuthorizationController;
use App\Entity\Data;
use App\Entity\Users;
use App\Form\DataType;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class DataController
 * @package App\Controller
 */
class DataController extends AbstractController implements AuthorizationController
{
    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(DocumentManager $dm, ValidatorInterface $validator)
    {
        $this->dm = $dm;
        $this->validator = $validator;
    }

    /**
     * Get data
     * @Route("/data/{id}", methods="GET", name="show_data")
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function show(Request $request, $id)
    {
        $data = $this->dm->getRepository('App\Entity\Data')->find($id);

        $user = $this->dm->getRepository(Users::class)
                    ->findUserByTokenOrFail($request->headers->get('Authorization'));

        if (!$data instanceof Data) {
            throw new NotFoundHttpException(sprintf('Data with id %s not found', $id));
        }

        if (!$this->checkPermission($user, $data)) {
            throw new AccessDeniedHttpException('You have no permission for this action');
        }

        // Normalize data object to array and return with JsonResponse
        return new JsonResponse($this->get('serializer')->normalize($data), JsonResponse::HTTP_OK);
    }

    /**
     * Create data
     * @Route("/data", methods="POST", name="create_data")
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $data = new Data();

        $user = $this->dm->getRepository(Users::class)
                    ->findUserByTokenOrFail($request->headers->get('Authorization'));

        $data_params = $request->request->all();
        $data_params['userId'] = $user->getId();

        $this->checkValid($data, $data_params);

        $this->dm->persist($data);
        $this->dm->flush();

        return new JsonResponse(sprintf('Data with id %s is created.', $data->getId()), JsonResponse::HTTP_CREATED);
    }

    /**
     * Update data
     * @Route("/data/{id}", methods="PUT", name="update_data")
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $data = $this->dm->getRepository(Data::class)->find($id);
        $user = $this->dm->getRepository(Users::class)
                    ->findUserByTokenOrFail($request->headers->get('Authorization'));

        $data_params = $request->request->all();
        $data_params['userId'] = $user->getId();

        if (!$data instanceof Data) {
            throw new NotFoundHttpException(sprintf('Data with id %s not found', $id));
        }

        if (!$this->checkPermission($user, $data)) {
            throw new AccessDeniedHttpException('You have no permission for this action');
        }

        $this->checkValid($data, $data_params);
        $this->dm->flush();

        return new JsonResponse(sprintf('Data with id %s is updated.', $data->getId()), JsonResponse::HTTP_OK);
    }

    /**
     * Delete data
     * @Route("/data/{id}", methods="DELETE", name="delete_data")
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function delete(Request $request, $id)
    {
        $data = $this->dm->getRepository('App\Entity\Data')->find($id);
        $user = $this->dm->getRepository(Users::class)
                    ->findUserByTokenOrFail($request->headers->get('Authorization'));

        if (!$data instanceof Data) {
            throw new NotFoundHttpException(sprintf('Data with id %s not found', $id));
        }

        if (!$this->checkPermission($user, $data)) {
            throw new AccessDeniedHttpException('You have no permission for this action');
        }

        $this->dm->remove($data);
        $this->dm->flush();

        return new JsonResponse(sprintf('User with id %s is deleted.', $data->getId()), JsonResponse::HTTP_OK);
    }

    /**
     * Check valid of Data entity
     * @param Data $data
     * @param array $parameters. Contain fields for Entity
     * @throws BadRequestHttpException
     * @return bool
     */
    private function checkValid(Data $data, array $parameters): bool
    {
        $form = $this->createForm(DataType::class, $data);
        $form->submit($parameters);

        // Validate entity object
        $validationResults = $this->validator->validate($data);

        // If validation errors occurs, return BAD_REQUEST (400) response
        if ($validationResults->count() > 0) {
            $errors = [];
            foreach ($validationResults as $violation) {
                $errors[$violation->getPropertyPath()] = $violation->getMessage();
            }
            throw new BadRequestHttpException(json_encode($errors));
        }

        return true;
    }

    /**
     * Check Users entity permission for Data entity
     * @param Users $user
     * @param Data $data
     * @return bool
     */
    private function checkPermission(Users $user, Data $data) : bool
    {
        return $user->getId() == $data->getUserId();
    }


}
