<?php

namespace App\Controller;

use App\Controller\Contracts\AdminAuthController;
use App\Entity\Users;
use App\Form\UsersType;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController implements AdminAuthController
{
    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(DocumentManager $dm, ValidatorInterface $validator)
    {
        $this->dm = $dm;
        $this->validator = $validator;
    }

    /**
     * Get user's information
     * @Route("/users/{id}", methods="GET", name="show_user")
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function show(Request $request, $id)
    {
        $user = $this->dm->getRepository(Users::class)->find($id);

        if (!$user instanceof Users) {
            throw new NotFoundHttpException(sprintf('User with id %s not found', $id));
        }

        // Normalize user object to array and return with JsonResponse
        return new JsonResponse($this->get('serializer')->normalize($user), Response::HTTP_OK);
    }

    /**
     * Create user
     * @Route("/users", methods="POST", name="create_user")
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $user = new Users();

        $this->checkValid($user, $request->request->all());

        $this->dm->persist($user);
        $this->dm->flush();

        return new JsonResponse(sprintf('User with id %s is created.', $user->getId()), Response::HTTP_CREATED);

    }

    /**
     * Update user's information
     * @Route("/users/{id}", methods="PUT", name="update_user")
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $user = $this->dm->getRepository(Users::class)->find($id);
        if (!$user instanceof Users) {
            throw new NotFoundHttpException(sprintf('User with id %s not found', $id));
        }

        $this->checkValid($user, $request->request->all());
        $this->dm->flush();

        return new JsonResponse(sprintf('User with id %s is updated.', $user->getId()), Response::HTTP_OK);
    }

    /**
     * Delete user
     * @Route("/users/{id}", methods="DELETE", name="delete_user")
     * @param $id
     * @return JsonResponse
     */
    public function delete($id)
    {
        $user = $this->dm->getRepository(Users::class)->find($id);

        if (!$user instanceof Users) {
            throw new NotFoundHttpException(sprintf('User with id %s not found', $id));
        }

        $this->dm->remove($user);
        $this->dm->flush();

        return new JsonResponse(sprintf('User with id %s is deleted.', $user->getId()), Response::HTTP_OK);
    }

    /**
     * Check valid of Users entity
     * @param Users $user
     * @param array $data . Contain fields for Entity
     * @throws BadRequestHttpException
     * @return bool
     */
    private function checkValid(Users $user, array $data): bool
    {
        $form = $this->createForm(UsersType::class, $user);
        $form->submit($data);

        // Validate entity object
        $validationResults = $this->validator->validate($user);

        // If validation errors occurs, return BAD_REQUEST (400) response
        if ($validationResults->count() > 0) {
            $errors = [];
            foreach ($validationResults as $violation) {
                $errors[$violation->getPropertyPath()] = $violation->getMessage();
            }
            throw new BadRequestHttpException(json_encode($errors));
        }

        return true;
    }
}