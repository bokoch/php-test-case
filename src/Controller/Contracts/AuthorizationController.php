<?php

namespace App\Controller\Contracts;

/**
 * Interface AuthorizationController
 * Implementing this interface to controller,
 * will check all requests for header `Authorization` with {userLogin}:{userPassword}
 * @package App\Controller
 */
interface AuthorizationController
{

}