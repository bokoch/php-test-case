<?php

namespace App\Controller\Contracts;

/**
 * Interface AdminAuthController
 * Implementing this interface to controller,
 * will check all requests for header Authorization with specialAdminToken
 * @package App\Controller
 */
interface AdminAuthController
{

}