<?php

namespace App\EventSubscriber;


use App\Controller\Contracts\AdminAuthController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Check all requests for attribute Authorization with admin token
 * Class AuthApiSubscriber
 * @package App\EventSubscriber
 */
class AdminAuthSubscriber implements EventSubscriberInterface
{
    private $specialAdminToken;

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        // Check if event controller instantiate AuthApiController
        if ($controller[0] instanceof AdminAuthController) {

            if (!$event->getRequest()->headers->has('Authorization')) {
                throw new UnauthorizedHttpException('Unauthorized', 'Special admin token is required for this action');
            }
            // Compare token with token from DB, etc.
            $token = $event->getRequest()->headers->get('Authorization');
        }
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'onKernelController',
        );
    }
}