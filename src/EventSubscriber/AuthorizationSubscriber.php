<?php

namespace App\EventSubscriber;


use App\Controller\Contracts\AuthorizationController;
use App\Entity\Users;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class AuthorizationSubscriber implements EventSubscriberInterface
{
    /**
     * @var DocumentManager
     */
    private $dm;

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        // Check if event controller instantiate AuthApiController
        if ($controller[0] instanceof AuthorizationController) {

            if (!$event->getRequest()->headers->has('Authorization')) {
                throw new UnauthorizedHttpException('Unauthorized', 'Authorization parameter is required for this action');
            }
            $authorization = $event->getRequest()->headers->get('Authorization');

            // get login and password from Authorization header
            // with template {userLogin}:{userPassword}
            $auth_array = explode(':', $authorization);
            if (count($auth_array) != 2) {
                throw new UnprocessableEntityHttpException('Authorization parameter must have format {userLogin}:{userPassword}');
            }

        }
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'onKernelController',
        );
    }
}