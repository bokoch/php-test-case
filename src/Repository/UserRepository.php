<?php

namespace App\Repository;

use App\Entity\Users;
use App\Repository\Contracts\UserRepositoryInterface;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Repository for Users entity
 * Class UserRepository
 * @package App\Repository
 */
class UserRepository extends DocumentRepository implements UserRepositoryInterface
{
    /**
     * Parse token with format {userLogin}:{userPassword}
     * @param $token
     * @throws NotFoundHttpException
     * @return null|object
     */
    public function findUserByTokenOrFail($token)
    {
        $auth_array = explode(':', $token);

        $userLogin = $auth_array[0];
        $userPassword = $auth_array[1];

        $query_params = [
            'login'=> $userLogin,
            'password'=> $userPassword
        ];

        // check if users with such credentials exist
        $user = $this->dm->getRepository(Users::class)
            ->findOneBy($query_params);

        if (!$user instanceof Users) {
            throw new NotFoundHttpException('User with such credentials not exist');
        }

        return $user;
    }
}