<?php

namespace App\Repository\Contracts;


/**
 * Interface UserRepositoryInterface
 * @package App\Repository\Contracts
 */
interface UserRepositoryInterface
{
    /**
     * Parse token with format {userLogin}:{userPassword}
     * @param $token
     */
    public function findUserByTokenOrFail($token);
}