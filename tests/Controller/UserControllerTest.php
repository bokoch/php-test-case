<?php

namespace App\Tests\Controller;

use App\Entity\Users;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Serializer\Serializer;

/**
 * Test for App\Controller\UserController
 * Class UserControllerTest
 * @package App\Tests\Controller
 */
class UserControllerTest extends WebTestCase
{
    /**
     * @var Users
     */
    private static $testUser;

    /**
     * @var DocumentManager
     */
    private static $dm;

    /**
     * @var Serializer
     */
    private static $serializer;

    /**
     * Create test user for GET, PUT and DELETE actions
     */
    public static function setUpBeforeClass()
    {
        $kernel = self::bootKernel();

        self::$dm = $kernel->getContainer()
            ->get('doctrine_mongodb')
            ->getManager();

        self::$serializer = $kernel->getContainer()
            ->get('serializer');

        $user = new Users();
        $user->setLogin('testLogin');
        $user->setPassword('testPassword');

        self::$dm->persist($user);
        self::$dm->flush();

        self::$testUser = $user;
    }

    /**
     * Remove test user if it exist
     */
    public static function tearDownAfterClass()
    {
        self::$dm->remove(self::$testUser);
        self::$dm->flush();
    }

    /**
     * Test for /users/{id} route
     */
    public function testShowAction()
    {
        $client = static::createClient();

        $headers = array(
            'HTTP_AUTHORIZATION' => "someToken",
            'CONTENT_TYPE' => 'application/json'
        );

        /** Check route without admin token */
        $client->request(Request::METHOD_GET, sprintf('/users/%s', self::$testUser->getId()));
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());

        /** Check route with admin token */
        $client->request(Request::METHOD_GET, sprintf('/users/%s', self::$testUser->getId()), array(), array(), $headers);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        /** Check equality of json strings from response with expected */
        $this->assertJsonStringEqualsJsonString(
            self::$serializer->serialize(self::$testUser, 'json'),
            $client->getResponse()->getContent()
        );

        /** Check not found user */
        $client->request(Request::METHOD_GET, '/users/111', array(), array(), $headers);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    /**
     * Test for /users route (method:POST, create users action)
     */
    public function testPostAction()
    {
        $client = static::createClient();

        $headers = array(
            'HTTP_AUTHORIZATION' => "someToken",
            'CONTENT_TYPE' => 'application/json'
        );

        $body = array(
            'login' => "test_login",
            'password' => 'pass123'
        );

        // login too long (max=32 symbols)
        $tooLongBody = array(
            'login' => "test_login-test_login-test_login-test_login",
            'password' => 'pass123'
        );

        /** Check route without admin token */
        $client->request(Request::METHOD_POST, '/users');
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());

        /** Check route without body, validation test */
        $client->request(Request::METHOD_POST, '/users', array(), array(), $headers);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());

        /** Check route with not valid login (too long) */
        $client->request(Request::METHOD_POST, '/users', $tooLongBody, array(), $headers);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());

        /** Check route with body */
        $client->request(Request::METHOD_POST, '/users', $body, array(), $headers);
        $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
    }

    /**
     * Test for /users/{id} route (method:PUT, update user action)
     */
    public function testPutAction()
    {
        $client = static::createClient();

        $headers = array(
            'HTTP_AUTHORIZATION' => "someToken",
            'CONTENT_TYPE' => 'application/json'
        );

        $body = array(
            'login' => "test_login",
            'password' => 'pass123'
        );

        /** Check route without admin token, take id of testUser */
        $client->request(Request::METHOD_PUT, sprintf('/users/%s', self::$testUser->getId()));
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());

        /** Check not found user. Response = 404 */
        $client->request(Request::METHOD_PUT, '/users/111', array(), array(), $headers);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());

        /** Check route without body, validation test. Response = 400 */
        $client->request(Request::METHOD_PUT, sprintf('/users/%s', self::$testUser->getId()), array(), array(), $headers);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());

        /** Check route with body. Response = 200 */
        $client->request(Request::METHOD_PUT, sprintf('/users/%s', self::$testUser->getId()), $body, array(), $headers);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    /**
     * Test for /users/{id} route (method:DELETE, delete user action)
     */
    public function testDeleteAction()
    {
        $client = static::createClient();

        $headers = array(
            'HTTP_AUTHORIZATION' => "someToken",
            'CONTENT_TYPE' => 'application/json'
        );

        /** Check route without admin token, take id of testUser */
        $client->request(Request::METHOD_DELETE, sprintf('/users/%s', self::$testUser->getId()));
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());

        /** Check not found user. Response = 404 */
        $client->request(Request::METHOD_DELETE, '/users/111', array(), array(), $headers);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());

        /** Check route with body. Response = 200 */
        $client->request(Request::METHOD_DELETE, sprintf('/users/%s', self::$testUser->getId()), array(), array(), $headers);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

}