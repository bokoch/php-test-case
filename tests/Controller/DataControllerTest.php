<?php

namespace App\Tests\Controller;


use App\Entity\Data;
use App\Entity\Users;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class DataControllerTest extends WebTestCase
{
    /**
     * User that have permission for data object
     * @var Users
     */
    private static $testUser;

    /**
     * User that haven't permission for data object
     * @var Users
     */
    private static $userNoPerm;

    /**
     * @var Data
     */
    private static $testData;

    /**
     * @var DocumentManager
     */
    private static $dm;

    /**
     * @var Serializer
     */
    private static $serializer;

    /**
     * Test Client
     */
    private static $client;

    /**
     * Not valid Authorization header
     */
    private static $badHeaders;

    /**
     * Headers with Authorization credentials that haven't permission for data
     */
    private static $noPermHeaders;

    /**
     * Valid Authorization header
     */
    private static $headers;
    

    /**
     * Create test data for GET, PUT and DELETE actions
     * @throws \Exception
     */
    public static function setUpBeforeClass()
    {
        $kernel = self::bootKernel();

        self::$dm = $kernel->getContainer()
            ->get('doctrine_mongodb')
            ->getManager();

        self::$serializer = $kernel->getContainer()
            ->get('serializer');

        // Create test data and users
        $user = new Users();
        // rand need for exclude repeat of credentials
        $user->setLogin('testLogin_' . random_int(100,999));
        $user->setPassword('testPassword');

        $userNoPerm = new Users();
        $userNoPerm->setLogin('testLoginNoPerm_' . random_int(100,999));
        $userNoPerm->setPassword('testPasswordNoPerm');

        self::$dm->persist($user);
        self::$dm->persist($userNoPerm);
        self::$dm->flush();
        self::$testUser = $user;
        self::$userNoPerm = $userNoPerm;

        $data = new Data();
        $data->setKey('testKey');
        $data->setData('testData');
        $data->setUserId($user->getId());

        self::$dm->persist($data);
        self::$dm->flush();

        self::$testData = $data;

        self::$client = self::createClient();

        // header with bad Authorization token
        self::$badHeaders = array(
            'HTTP_AUTHORIZATION' => self::$testUser->getLogin(),
            'CONTENT_TYPE' => 'application/json'
        );

        self::$noPermHeaders = array(
            'HTTP_AUTHORIZATION' => self::$userNoPerm->getLogin() . ':' . self::$userNoPerm->getPassword(),
            'CONTENT_TYPE' => 'application/json'
        );

        self::$headers = array(
            'HTTP_AUTHORIZATION' => self::$testUser->getLogin() . ':' . self::$testUser->getPassword(),
            'CONTENT_TYPE' => 'application/json'
        );
    }

    /**
     * Remove test data if it exist
     */
    public static function tearDownAfterClass()
    {
        self::$dm->remove(self::$testData);
        self::$dm->remove(self::$userNoPerm);
        self::$dm->remove(self::$testData);
        self::$dm->flush();
    }

    /**
     * Test for /data/{id} route (method GET)
     */
    public function testShowAction()
    {
        /** Check route without Authorization token */
        self::$client->request(Request::METHOD_GET, sprintf('/data/%s', self::$testData->getId()));
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, self::$client->getResponse()->getStatusCode());

        /** Check route with not valid token, not {userLogin}:{userPassword} */
        self::$client->request(Request::METHOD_GET, sprintf('/data/%s', self::$testData->getId()), array(), array(), self::$badHeaders);
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, self::$client->getResponse()->getStatusCode());

        /** Check route with user that haven't permission for data entity */
        self::$client->request(Request::METHOD_GET, sprintf('/data/%s', self::$testData->getId()), array(), array(), self::$noPermHeaders);
        $this->assertEquals(Response::HTTP_FORBIDDEN, self::$client->getResponse()->getStatusCode());

        /** Check route with valid token, like {userLogin}:{userPassword} */
        self::$client->request(Request::METHOD_GET, sprintf('/data/%s', self::$testData->getId()), array(), array(), self::$headers);
        $this->assertEquals(Response::HTTP_OK, self::$client->getResponse()->getStatusCode());

        /** Check equality of json strings from response with expected */
        $this->assertJsonStringEqualsJsonString(
            self::$serializer->serialize(self::$testData, 'json'),
            self::$client->getResponse()->getContent()
        );

        /** Check not found data */
        self::$client->request(Request::METHOD_GET, '/data/111', array(), array(), self::$headers);
        $this->assertEquals(Response::HTTP_NOT_FOUND, self::$client->getResponse()->getStatusCode());

    }

    /**
     * Test for /data route (method POST)
     */
    public function testPostAction()
    {
        // header with Authorization token that get 404 for user
        $notFoundHeaders = array(
            'HTTP_AUTHORIZATION' => '1:1',
            'CONTENT_TYPE' => 'application/json'
        );

        $body = array(
            'key' => "key123",
            'data' => 'data123'
        );

        $tooLongBody = array(
            'key' => "key123-key123-key123-key123-key123-key123-key123-key123",
            'data' => 'data123'
        );

        /** Check route without Authorization token */
        self::$client->request(Request::METHOD_POST, '/data');
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, self::$client->getResponse()->getStatusCode());

        /** Check route with not valid token, not {userLogin}:{userPassword} */
        self::$client->request(Request::METHOD_POST, '/data', array(), array(), self::$badHeaders);
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, self::$client->getResponse()->getStatusCode());

        /** Check route with that not existing credentials */
        self::$client->request(Request::METHOD_GET, sprintf('/data/%s', self::$testData->getId()), array(), array(), $notFoundHeaders);
        $this->assertEquals(Response::HTTP_NOT_FOUND, self::$client->getResponse()->getStatusCode());

        /** Check route without body */
        self::$client->request(Request::METHOD_POST, '/data', array(), array(), self::$headers);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, self::$client->getResponse()->getStatusCode());

        /** Check route with not valid key (too long) */
        self::$client->request(Request::METHOD_POST, '/users', $tooLongBody, array(), self::$headers);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, self::$client->getResponse()->getStatusCode());

        /** Check route with valid body */
        self::$client->request(Request::METHOD_POST, '/data', $body, array(), self::$headers);
        $this->assertEquals(Response::HTTP_CREATED, self::$client->getResponse()->getStatusCode());

    }

    /**
     * Test for /data/{id} route (method PUT)
     */
    public function testPutAction()
    {
        $body = array(
            'key' => "keyChange",
            'data' => 'dataChange'
        );

        /** Check route without Authorization token */
        self::$client->request(Request::METHOD_PUT, sprintf('/data/%s', self::$testData->getId()));
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, self::$client->getResponse()->getStatusCode());

        /** Check route with not valid token, not {userLogin}:{userPassword} */
        self::$client->request(Request::METHOD_PUT, sprintf('/data/%s', self::$testData->getId()), array(), array(), self::$badHeaders);
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, self::$client->getResponse()->getStatusCode());

        /** Check route with user that haven't permission for data entity */
        self::$client->request(Request::METHOD_PUT, sprintf('/data/%s', self::$testData->getId()), array(), array(), self::$noPermHeaders);
        $this->assertEquals(Response::HTTP_FORBIDDEN, self::$client->getResponse()->getStatusCode());

        /** Check route with valid token, like {userLogin}:{userPassword} */
        self::$client->request(Request::METHOD_PUT, sprintf('/data/%s', self::$testData->getId()), $body, array(), self::$headers);
        $this->assertEquals(Response::HTTP_OK, self::$client->getResponse()->getStatusCode());

        /** Check not found data */
        self::$client->request(Request::METHOD_PUT, '/data/111', array(), array(), self::$headers);
        $this->assertEquals(Response::HTTP_NOT_FOUND, self::$client->getResponse()->getStatusCode());

    }

    /**
     * Test for /data/{id} route (method DELETE)
     */
    public function testDeleteAction()
    {
        /** Check route without Authorization token */
        self::$client->request(Request::METHOD_DELETE, sprintf('/data/%s', self::$testData->getId()));
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, self::$client->getResponse()->getStatusCode());

        /** Check route with not valid token, not {userLogin}:{userPassword} */
        self::$client->request(Request::METHOD_DELETE, sprintf('/data/%s', self::$testData->getId()), array(), array(), self::$badHeaders);
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, self::$client->getResponse()->getStatusCode());

        /** Check route with user that haven't permission for data entity */
        self::$client->request(Request::METHOD_DELETE, sprintf('/data/%s', self::$testData->getId()), array(), array(), self::$noPermHeaders);
        $this->assertEquals(Response::HTTP_FORBIDDEN, self::$client->getResponse()->getStatusCode());

        /** Check route with valid token, like {userLogin}:{userPassword} */
        self::$client->request(Request::METHOD_DELETE, sprintf('/data/%s', self::$testData->getId()), array(), array(), self::$headers);
        $this->assertEquals(Response::HTTP_OK, self::$client->getResponse()->getStatusCode());

        /** Check not found data */
        self::$client->request(Request::METHOD_DELETE, '/data/111', array(), array(), self::$headers);
        $this->assertEquals(Response::HTTP_NOT_FOUND, self::$client->getResponse()->getStatusCode());

    }

}